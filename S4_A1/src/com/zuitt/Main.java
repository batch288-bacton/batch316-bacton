package com.zuitt;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Pedro Penduko", 36, "amazingPedro@mail.com", "Purok - Tikbalang, Floating Village, Encantadia");

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

        Date startDate = new Date(123, 10, 1); // November 1, 2023
        Date endDate = new Date(123, 10, 30); // November 30, 2023

        Course course1 = new Course("Exorcism101", "Introduction to Exorcism and how to fight demons", 10, 666.999, startDate, endDate, user1);

        System.out.println("Hi! I'm " + user1.getName() + ". I'm " + user1.getAge() + ". You can reach me via my email: " + user1.getEmail());
        System.out.println("When I'm off work, I can be found at my house in " + user1.getAddress() + " drinking my sweet hot coffee brewed from the ancient world tree that I slash when I was 9.");

        System.out.println("Welcome to the course " + course1.getName() + ".");
        System.out.println("This course can be described as " + course1.getDescription() + " for witch-hunters.");
        System.out.println("This course will start on " + dateFormat.format(course1.getStartDate()) + " till " + dateFormat.format(course1.getEndDate()) + ".");
        System.out.println("Your instructor for this course is the AMAZING Sir " + user1.getName() + ".");
        System.out.println("Enjoy!");
    }
}
