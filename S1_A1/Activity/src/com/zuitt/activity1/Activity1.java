package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {

        Scanner myGradeAve = new Scanner(System.in);
        System.out.println("First Name: ");
        String firstName = myGradeAve.nextLine();

        System.out.println("Last Name: ");
        String lastName = myGradeAve.nextLine();

        System.out.println("First Subject Grade: ");
        int firstSubjectGrade = Integer.parseInt(myGradeAve.nextLine());

        System.out.println("Second Subject Grade: ");
        int secondSubjectGrade = Integer.parseInt(myGradeAve.nextLine());

        System.out.println("Third Subject Grade: ");
        int thirdSubjectGrade = Integer.parseInt(myGradeAve.nextLine());

        System.out.println("Fourth Subject Grade: ");
        int fourthSubjectGrade = Integer.parseInt(myGradeAve.nextLine());

        int sumGrades = firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade + fourthSubjectGrade;
        double aveGrade = (double) sumGrades / 4;

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + aveGrade);
    }
}
