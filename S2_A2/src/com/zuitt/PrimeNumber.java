package com.zuitt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeArrayNumbers = new int[5];

        //Activity1 on S2_A2
        primeArrayNumbers[0] = 2;
        primeArrayNumbers[1] = 3;
        primeArrayNumbers[2] = 5;
        primeArrayNumbers[3] = 7;
        primeArrayNumbers[4] = 11;

        System.out.println("The first prime number is: " + primeArrayNumbers[0]);

        //Activity2 on S2_A2
        ArrayList<String> friendsList = new ArrayList<>();

        friendsList.add("John");
        friendsList.add("Jane");
        friendsList.add("Chloe");
        friendsList.add("Zoey");

        System.out.println("My friends are: " + friendsList);

        //Activity3 on S2_A2
        Map<String, Integer> inventoryMap = new HashMap<>();

        inventoryMap.put("toothpaste", 15);
        inventoryMap.put("toothbrush", 20);
        inventoryMap.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventoryMap);

    }
}
