package com.zuitt;
import java.util.ArrayList;
import java.util.List;

public class Contact {
    private String name;
    private List<String> contactNumbers;
    private List<String> addresses;

    public Contact(){
        contactNumbers = new ArrayList<>();
        addresses = new ArrayList<>();
    }

    public Contact(String name, String contactNumber1, String contactNumber2, String address1, String address2){
        this.name = name;
        contactNumbers = new ArrayList<>();
        addresses = new ArrayList<>();
        contactNumbers.add(contactNumber1);
        contactNumbers.add(contactNumber2);
        addresses.add(address1);
        addresses.add(address2);
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public List<String> getContactNumbers(){
        return contactNumbers;
    }
    public void setContactNumbers(List<String> contactNumbers){
        this.contactNumbers = contactNumbers;
    }

    public List<String> getAddresses(){
        return addresses;
    }
    public void setAddresses(List<String> addresses){
        this.addresses = addresses;
    }

    public String getDetails(){
        StringBuilder details = new StringBuilder();
        details.append(name).append("\n");
        details.append("--------------------").append("\n");
        details.append(name).append(" has the following registered numbers:").append("\n");
        for (String contactNumber : contactNumbers) {
            details.append(contactNumber).append("\n");
        }
        details.append("--------------------").append("\n");
        details.append(name).append(" has the following registered addresses:").append("\n");
        for (String address : addresses) {
            details.append(address).append("\n");
        }
        details.append("===============================================");
        return details.toString();
    }
}
