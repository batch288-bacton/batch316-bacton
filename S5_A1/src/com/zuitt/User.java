package com.zuitt;

public class User {
    private String name;
    private int age;
    private String email;
    private String address;

    public User(){
    }

    public User(String name, int age, String email, String address){
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String nameParams){
        this.name = nameParams;
    }

    public int getAge(){
        return this.age;
    }
    public void setAge(int ageParams){
        this.age = ageParams;
    }

    public String getEmail(){
        return this.email;
    }
    public void setEmail(String emailParams){
        this.email = emailParams;
    }

    public String getAddress(){
        return this.address;
    }
    public void setAddress(String addressParams){
        this.address = addressParams;
    }
}
