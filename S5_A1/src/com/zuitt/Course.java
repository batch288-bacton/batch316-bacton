package com.zuitt;
import java.util.Date;

public class Course {
    private String name;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;
    private Date endDate;
    private User instructor;

    public Course(String name, String description, int seats, double fee, Date startDate, Date endDate, User instructor){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String nameParams){
        this.name = nameParams;
    }

    public String getDescription(){
        return this.description;
    }
    public void setDescription(String descriptionParams){
        this.description = descriptionParams;
    }

    public int getSeats(){
        return this.seats;
    }
    public void setSeats(int seatsParams){
        this.seats = seatsParams;
    }

    public double getFee(){
        return this.fee;
    }
    public void setFee(double feeParams){
        this.fee = feeParams;
    }

    public Date getStartDate(){
        return this.startDate;
    }
    public void setStartDate(Date startDateParams){
        this.startDate = startDateParams;
    }

    public Date getEndDate(){
        return this.endDate;
    }
    public void setEndDate(Date endDateParams){
        this.endDate = endDateParams;
    }
}
