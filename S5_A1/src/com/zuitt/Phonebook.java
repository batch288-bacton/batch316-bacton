package com.zuitt;
import java.util.ArrayList;
import java.util.List;

public class Phonebook {
    private List<Contact> contacts;
    public Phonebook(){
        this.contacts = new ArrayList<>();
    }
    public Phonebook(List<Contact> contacts){
        this.contacts = contacts;
    }
    public List<Contact> getContacts(){
        return contacts;
    }
    public void setContacts(List<Contact> contacts){
        this.contacts = contacts;
    }
    public void addContacts(Contact contact){
        contacts.add(contact);
    }
    public boolean isEmpty() {
        return contacts.isEmpty();
    }
}
