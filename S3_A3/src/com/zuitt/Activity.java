package com.zuitt;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed: ");
        Scanner input = new Scanner(System.in);

        try {
            int num = input.nextInt();

            if (num < 0) {
                System.out.println("Factorial is not defined for negative numbers.");
            } else if (num == 0) {
                System.out.println("Factorial of 0 is 1.");
            } else {
                // Calculate factorial using a while loop
                int counter = 1;
                int answer = 1;

                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }

                System.out.println("The factorial of " + num + " using while loop: " + answer);

                // Calculate factorial using a for loop
                answer = 1;

                for (int i = 1; i <= num; i++) {
                    answer *= i;
                }

                System.out.println("The factorial of " + num + " using for loop: " + answer);
            }
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter an integer.");
        }

        input.close();
    }
}

